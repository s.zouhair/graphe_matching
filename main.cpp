#include <iostream>
#include <fstream>
#include <cassert>
#include "Edmonds.hpp"
using namespace std;

int main(int argc, char **argv)
{
	Graph Graphe;
	int Choice = atoi(argv[1]);
	if (Choice == 0)
	{
		// Choice = 0 si l'utilisateur veut utiliser le graphe décrit dans le fichier graph.txt
		cout << " Chargement du graphe à partir du fichier graph.txt" << endl;
		Graph Graphe("../graph.txt");
		cout << "La taille du graphe est : " << Graphe.GraphSize() << endl;
		Graphe.DisplayGraph();
		cout << " On va construire maintenant le couplage maximale du graphe " << endl; 
		FindMaximumMatching(Graphe);
		cout << " On enregistre les résultats dans le fichier GraphMatvhing.txt " << endl;
		ExportGraph(Graphe);
	}
	else
	{
		// Choice != 0 si l'utilisateur veut utiliser un graphe aléatoire ayant un nombre de sommets égale à "Choice", et au moins "MinEdgesNbr" arete par sommet.
		int MinEdgesNbr = atoi(argv[2]);
		assert (Choice > 1 && MinEdgesNbr > 0);
		cout << "Création d'un graphe de " << Choice << " sommets, et ayant au moins " << MinEdgesNbr << " aretes par sommet. " << endl;
		Graph Graphe(Choice , MinEdgesNbr);
		cout << "La taille du graphe est : " << Graphe.GraphSize() << endl;
		Graphe.DisplayGraph();
		cout << " On va construire maintenant le couplage maximale du graphe " << endl; 
		FindMaximumMatching(Graphe);
		cout << " On enregistre les résultats dans le fichier GraphMatvhing.txt " << endl;
		ExportGraph(Graphe);
	}
	/*cout << " On va maintenant afficher le graphe avec le nouveau couplage maximale " << endl; 
	G.DisplayGraph();*/
	return 0;
}











