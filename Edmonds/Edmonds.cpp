#include <iostream>
#include <cassert>
#include <fstream>
#include "Edmonds.hpp"
#include <time.h>
using namespace std;


Graph::~Graph()
{
	cout << "On supprime un graphe" << endl;
	for (auto i = 0; i < VerticesNbr; i++)
	{
  		delete [] GraphEdges[i];
	}
	delete [] GraphEdges;
	delete [] Vertices_State; // je viens de la modifier
	if (OldIndex != NULL) {delete [] OldIndex;} // aussi
}

Graph::Graph()
{
	VerticesNbr = 0;  	
	GraphEdges = NULL;
	Vertices_State = NULL;
	ExposedVetricesNbr = 0;
	OldIndex = NULL;
}

Graph::Graph(const std::string GraphPath)
{
	std::ifstream file(GraphPath);
	file >> VerticesNbr;
	ExposedVetricesNbr = VerticesNbr;
	GraphEdges = new int*[VerticesNbr];
	Vertices_State = new int[VerticesNbr];
	assert (GraphEdges != NULL && Vertices_State != NULL);
	OldIndex = NULL;
	for (auto i = 0; i < VerticesNbr; i++)
	{
		Vertices_State[i] = 0; // Déclarer le sommet numéro i exposé
		int Edges_Nbr = 0;
  		GraphEdges[i] = new int[VerticesNbr];
		assert (GraphEdges[i]);
		for (auto j = 0; j < VerticesNbr; j++)
		{
			file >> GraphEdges[i][j];
			if (GraphEdges[i][j] != 0) //Attention : il faut ajouter "and i != j"
			{
				Edges_Nbr++;
				if (GraphEdges[i][j] % 3 == 0)
				{
					Vertices_State[i] = j+1; // Le sommet "i" n'est plus exposé parce que l'arête qui le lie au sommet "j" fait partie du couplage
					ExposedVetricesNbr--;
				}
			}
		}
		GraphEdges[i][i] = Edges_Nbr;
	}
}

Graph::Graph(int Vertices_Nbr , int MinEdgesNbr)
{
	VerticesNbr = Vertices_Nbr;
	GraphEdges = new int*[VerticesNbr];
	Vertices_State = new int[VerticesNbr];
	assert (GraphEdges != NULL && Vertices_State != NULL);
	OldIndex = NULL;
	ExposedVetricesNbr = VerticesNbr;
	for (auto i = 0; i < VerticesNbr; i++)
	{
		GraphEdges[i] = NULL;
		GraphEdges[i] = new int[VerticesNbr];
		assert(GraphEdges[i] != NULL);
		Vertices_State[i] = 1;
		for (auto j = 0; j < VerticesNbr; j++)
		{
			GraphEdges[i][j] = 0;
		}
	}
	srand(time(NULL));
	int RemainingVertex = VerticesNbr;
	int TotalEdgesNbr = 0;
	int TestNbr;
	int ChosenVertex = 0;
	int SecondVertex = 0;
	while (RemainingVertex > 0)
	{
		ChosenVertex = rand() % VerticesNbr;
		TestNbr = 0;
		while ( GraphEdges[ChosenVertex][ChosenVertex] < MinEdgesNbr && TestNbr < 100*VerticesNbr )
		{
			SecondVertex = rand() % VerticesNbr;
			TestNbr++;
			if ( ChosenVertex != SecondVertex && GraphEdges[ChosenVertex][SecondVertex] == 0 && GraphEdges[SecondVertex][SecondVertex] <= 2*TotalEdgesNbr/VerticesNbr) // "2*TotalEdgesNbr/VerticesNbr" Chaque arete appartient a 2 sommets
			{
				GraphEdges[ChosenVertex][SecondVertex] = 1;
				GraphEdges[SecondVertex][ChosenVertex] = 1;
				GraphEdges[ChosenVertex][ChosenVertex] += 1;
				GraphEdges[SecondVertex][SecondVertex] += 1;
				TotalEdgesNbr++;
			}
		}
		while ( GraphEdges[ChosenVertex][ChosenVertex] < MinEdgesNbr )
		{
			SecondVertex = rand() % VerticesNbr;
			if ( ChosenVertex != SecondVertex && GraphEdges[ChosenVertex][SecondVertex] == 0 )
			{
				GraphEdges[ChosenVertex][SecondVertex] = 1;
				GraphEdges[SecondVertex][ChosenVertex] = 1;
				GraphEdges[ChosenVertex][ChosenVertex] += 1;
				GraphEdges[SecondVertex][SecondVertex] += 1;
				TotalEdgesNbr++;
			}
		}
		if (Vertices_State[ChosenVertex] == 1)
		{
			Vertices_State[ChosenVertex] = 0;
			RemainingVertex -= 1;
		}
	}
}

std::size_t Graph::GraphSize() const
{
	return VerticesNbr;
}

std::size_t Graph::GetExpNbr() const
{
	return ExposedVetricesNbr;
}

void Graph::SetExpNbr(int Nbr) // A vérifier
{
	ExposedVetricesNbr = Nbr;
}

void Graph::BuildGraph(int Nbr) // A vérifier
{
	assert (GraphEdges == NULL && VerticesNbr == 0 && Vertices_State == NULL);
	VerticesNbr = Nbr;
	Vertices_State = new int[VerticesNbr];
	OldIndex = new int[VerticesNbr-1];
	GraphEdges = new int*[Nbr];
	for (auto i = 0; i < Nbr; i++)
	{
		GraphEdges[i] = new int[Nbr];
	}
}

void Graph::DisplayGraph() // fonction qui permet d'afficher le graph
{
	cout << "Le nombre de sommets de ce graphe est : " << GraphSize() << endl;
	cout << " le tableau des arêtes est :" << endl;
	for(auto i = 0 ; i < GraphSize() ; i++)
	{
		for(auto j = 0 ; j < GraphSize() ; j++)
		{
	  		cout << GraphEdges[i][j] << " " ;
		}
		cout << endl ;
	}
	cout << " le tableau représentant l'état des sommets est :" << endl;
	for(auto i = 0 ; i < GraphSize() ; i++)
	{
		cout << Vertices_State[i] ;
	}
	cout << endl ;
	cout << "Le nombre des sommets exposés est : " << ExposedVetricesNbr << endl ;
	if (OldIndex != NULL)
	{
		cout << " le tableau des anciens indices est :" << endl;
		for(auto j = 1 ; j <= GraphSize()-1 ; j++)
		{
	  		cout << this->OldInd(j) << " " ;
		}
		cout << endl ;
	}
}

int& Graph::VerticesState(const std::size_t indice)
{
	assert ( indice > 0 && indice <= VerticesNbr);
	return Vertices_State[indice-1];
}

int& Graph::operator()(const std::size_t indice_i, const std::size_t indice_j)
{
	assert ( indice_i > 0 && indice_i <= VerticesNbr && indice_j > 0 && indice_j <= VerticesNbr );
	return GraphEdges[indice_i-1][indice_j-1];
}

const int& Graph::operator()(const std::size_t indice_i, const std::size_t indice_j) const
{
	assert ( indice_i > 0 && indice_i <= VerticesNbr && indice_j > 0 && indice_j <= VerticesNbr );
	return GraphEdges[indice_i-1][indice_j-1];
}

void Graph::IncreaseMatching(const int& indice_i, const int& indice_j)
{
	assert ( indice_i > 0 && indice_i <= VerticesNbr && indice_j > 0 && indice_j <= VerticesNbr && GraphEdges[ indice_j-1 ][ indice_i-1 ] % 3 != 0 ); // S'assurer que les sommets ne sont pas couplés
	if ( VerticesState( indice_i ) != 0 )
	{
		DecreaseMatching( indice_i );
	}
	if ( VerticesState( indice_j ) != 0 )
	{
		DecreaseMatching( indice_j );
	}
	GraphEdges[ indice_i-1 ][ indice_j-1 ] += 2;
	GraphEdges[ indice_j-1 ][ indice_i-1 ] += 2;
	VerticesState( indice_i ) = indice_j;
	VerticesState( indice_j ) = indice_i;
	ExposedVetricesNbr -= 2;
	
}

void Graph::DecreaseMatching(const int& indice_i)
{
	int indice_j = VerticesState(indice_i);
	assert (indice_i > 0 && indice_i <= VerticesNbr && indice_j != 0 && GraphEdges[ indice_i-1 ][ indice_j-1 ] % 3 == 0); // S'assurer que le sommet indice_i est couplé
	GraphEdges[ indice_i-1 ][ indice_j-1 ] -= 2;
	GraphEdges[ indice_j-1 ][ indice_i-1 ] -= 2;
	VerticesState( indice_j ) = 0;
	VerticesState( indice_i ) = 0;
	ExposedVetricesNbr += 2;
	
}

int& Graph::OldInd(int indice)
{
	assert(indice > 0 && indice < VerticesNbr && OldIndex != NULL);
	return OldIndex[indice-1];
}

Vertex::Vertex()
{
	EdgesNbr = 0;
	Index = 0;
	VertexEdges = new int*[4];
	for (auto j = 0; j < 4; j++)
	{
		VertexEdges[j] = NULL;
	}
	VertexTree[0] = 0;
	VertexTree[1] = 0;
	VertexTree[2] = 0;
	Father = NULL;
}

Vertex::Vertex(const Graph& graph, const std::size_t i) // i doit etre >= 1
{
	EdgesNbr = graph(i,i);
	int ind = 0;
	Index = i;
	VertexTree[0] = 0;
	VertexTree[1] = 0;
	VertexTree[2] = 0;
	Father = NULL;
	VertexEdges = new int*[4];
	for (auto j = 0; j < 4; j++)
	{
		VertexEdges[j] = new int[EdgesNbr];
		assert ( VertexEdges[j] );
	}

	for (auto j = 1; j <= graph.GraphSize(); j++)
	{
		if ( graph(i,j) != 0 && i != j)
		{
			VertexEdges[0][ind] = j; // Les indices des sommet lié à "Vertex" par des aretes.
			VertexEdges[1][ind] = 0; // Si l'arete est marqué "1" ou non "0".
			VertexEdges[2][ind] = 0; // Si le sommet ne fait pas partie du meme arbre que le sommet courant "0". Si non, il est soit père "1" soit fils "2".
			VertexEdges[3][ind] = 0; // Si il y'a un sommet non marker par cette direction "1".
			ind++;
		}
	}
}

Vertex::~Vertex()
{
	for (auto j = 0; j < 4; j++)
	{
		delete [] VertexEdges[j];
	}
	delete [] VertexEdges; //aussi
}

std::size_t Vertex::VertexIndex()
{
	return Index;
}

int Vertex::Root()
{
	return VertexTree[0];
}

int Vertex::DistanceToRoot()
{
	return VertexTree[1];
}

void Vertex::AddSon(Vertex& V)
{
	for (auto i = 0; i < EdgesNbr; i++)
	{
		if (VertexEdges[0][i] == V.VertexIndex() /*  ||  VertexEdges[4][i] == V.VertexIndex() */)
		{
			VertexEdges[2][i] = 2; // V est un fils du sommet courant
			break;
		}
	}
}

void Vertex::AddFather(Vertex& V)
{
	Father = &V;
	for (auto i = 0; i < EdgesNbr; i++)
	{
		if (VertexEdges[0][i] == V.VertexIndex())
		{
			VertexEdges[2][i] = 1; // V est un père du sommet courant
			break;
		}
	}
	V.AddSon((*this));
}

void Vertex::AddTotree(Vertex& V) // On donne en argument le père
{
	if (V.VertexIndex() == Index) // Ca veut dire qu'on déclare un nouveau arbre
	{
		VertexTree[0] = Index;
		VertexTree[1] = 0; // outer
		VertexTree[2] = 0; // non marqué dans l'arbre
	}
	else // Pour ajouter le sommet à un arbre déja déclarer
	{
		VertexTree[0] = V.Root();
		VertexTree[1] = V.DistanceToRoot()+1;
		if (V.DistanceToRoot()%2 == 0)
		{
			VertexTree[2] = 1; // Une sommet inner est marqué par défaut
		}
		else  
		{
			V.UnMarkedDirect(*this);
			MarkEdge(V.VertexIndex()); // L'arete qui lie un sommet outer avec son sommet père est marquée par défaut
		} 
		this->AddFather(V);
	}
}


Vertex* Vertex::GetFather() // Pas encore utilisée
{
	assert ( Father != NULL );
	return Father;
}

void Vertex::MarkVertex()
{
	VertexTree[2] = 1;
	bool verif = true;
	for (auto i = 0; i < EdgesNbr; i++) // on verifie s'il y a une direction non marqué à partir du sommet courant qu'on va marqué sinon on va reporter à son père que la direction du sommet courant n'est plus non marqué
	{
		if ( VertexEdges[3][i] == 1 )
		{
			verif = false;
			break;
		}
	}
	if ( Father != NULL && verif )
	{
		Father->MarkedDirect(*this);
	}
}

void Vertex::UnMarkedDirect(Vertex& V) // La direction de l'arete V à partir du sommet courant, contient un sommet non marqué, le synthaxe est donc (père).UnMarkedDirect(fils)
{
	bool verif = true;
	for (auto i = 0; i < EdgesNbr; i++)
	{
		if (VertexEdges[3][i] == 1 && verif)
		{
			verif = false;
		}
		if (VertexEdges[0][i] == V.VertexIndex())
		{
			VertexEdges[3][i] = 1;
		}
	}
	if (verif && Index != VertexTree[0] && Father != NULL) // Si c la 1er fois que l'un des fils du sommet courant (qui est différent du root) reporte une direction non marquée, le sommet courant report de son coté une direction non marquée à son père.  
	{
		Father->UnMarkedDirect(*this);
	}
}

void Vertex::MarkedDirect(Vertex& V) // La direction de l'arete V à partir du sommet courant, ne contient plus un sommet non marqué, le synthaxe est donc (père).MarkedDirect(fils)
{
	bool verif = true;
	for (auto i = 0; i < EdgesNbr; i++)
	{
		if (VertexEdges[0][i] == V.VertexIndex())
		{
			VertexEdges[3][i] = 0;
		}
		if (VertexEdges[3][i] == 1 && verif)
		{
			verif = false;
		}
	}
	if (verif && Index != VertexTree[0] && Father != NULL) 
	{
		Father->MarkedDirect(*this);
	}
}

int Vertex::GetUnMarkedEdge() // elle revoit l'indice décalé
{
	int result = 0;
	for (auto i = 0; i < EdgesNbr; i++) // on peut utiliser une boucle while c mieux
	{
		if (VertexEdges[1][i] == 0 && result == 0)
		{
			result = VertexEdges[0][i];
		}
	}
	return result;
}

void Vertex::MarkEdge(int indice) // On donne en argument l'indice de l'extrémité différente du sommet courant de l'arete à marqué
{
	for (auto i = 0; i < EdgesNbr; i++)
	{
		if (VertexEdges[0][i] == indice)
		{
			VertexEdges[1][i] = 1;
			break;
		}
	}
}

int Vertex::GetUnMarkedVertex() // elle renvoit soit l'indice du sommet courant s'il est non marqué, soit celui de la direction où l'on peut trouver, soit 0.
{
	if (VertexTree[2] == 0) // Si le sommet courant est non marqué, la fonction renvoit son indice décalé
	{
		return Index;
	}
	else // Sinon elle renvoit l'indice de la direction (sommet fils) qui contient un sommet non marqué ou 0 si elle ne trouve aucune direction
	{
		int result = 0;
		for (auto i = 0; i < EdgesNbr; i++)
		{
			if ( VertexEdges[3][i] == 1) // Si la direction du sommet d'indice décalé "VertexEdges[0][i]" contient un sommet non marqué, on renvoit "VertexEdges[0][i]"
			{
				assert ( VertexEdges[2][i] == 2); // S'assurer que c'est bien un sommet fils
				result = VertexEdges[0][i];
				break;
			}
		}
		return result;
	}
}

void Vertex::ClearVertex()
{
	for (auto i = 0 ; i < EdgesNbr ; i++)
	{
		VertexEdges[1][i] = 0; // la 2eme ligne permet de savoir si l'arete est marqué ou non.
		VertexEdges[2][i] = 0; // la 3eme si le sommet ne fait pas partie du meme arbre "0", si oui, il est soit père "1" soit fils "2".
		VertexEdges[3][i] = 0; //  Et la 4ème si il y'a un sommet non marker par cette direction.
	}
	VertexTree[0] = 0; // Si le sommet appartient à un arbre, la 1er case de ce tableau va contenir son origine (root)
	VertexTree[1] = 0; // si le sommet est à une distance pair (outer) "0" ou impaire (inner)
	VertexTree[2] = 0; // Si le sommet est marqué ou non.
	
	Father = NULL;
}

Forest::Forest(Graph& G)
{
	VerticesNbr = G.GraphSize();
	ExposedVetricesNbr = G.GetExpNbr();
	VertexSet = NULL;
	Trees = NULL;
	Trees = new int[ExposedVetricesNbr];
	VertexSet = new Vertex*[VerticesNbr];
	assert ( VertexSet != NULL && Trees != NULL );
	int j = 0;
	for (auto i = 0; i < VerticesNbr; i++)
	{
		VertexSet[i] = new Vertex(G , i+1);
		if (G.VerticesState(i+1) == 0)
		{
			Trees[j] = i+1;
			j++;
		}
	}
	for (auto i = 0; i < ExposedVetricesNbr; i++)
	{
		GetVertex(Trees[i]).AddTotree(GetVertex(Trees[i]));
	}

}

Forest::~Forest()
{
	delete [] Trees;
	for (auto i = 0; i < VerticesNbr; i++ )
	{
		delete VertexSet[i]; // aussi
	}
	delete [] VertexSet; //aussi modifier 
}

Vertex& Forest::GetVertex(const std::size_t indice) // Cette fonction revoit une référence sur le sommet ayant l'indice décalé "indice"
{
	assert (indice > 0 && indice <= VerticesNbr);
	return *VertexSet[indice-1];
}

int Forest::GetUnMarkedVertex(Vertex& Sommet) // Cette fonction va renvoyer soit l'indice décalé du sommet non marqué soit 0 si elle n'a rien trouvée
{
	int result = Sommet.GetUnMarkedVertex();
	if (result != 0 && result != Sommet.VertexIndex())
	{
		return GetUnMarkedVertex(GetVertex(result));
	}
	else
	{
		return result;
	}
}

int Forest::GetUnMarkedVertex() // cette fonction va renvoyer soit l'indice décalé d'un sommet non marqué, soit 0 s'il n'y a aucun dans la foret
{
	int result = 0;
	for (auto i = 0 ; i < ExposedVetricesNbr ; i++)
	{
		result = GetUnMarkedVertex(GetVertex(Trees[i]));
		if (result != 0)
		{
			break;
		}
		// il faut ajouter un else car l'arbre est Hungarian dans ce cas
	}
	return result;
}

void Forest::UpdateForest(Graph& G)
{
	for (auto i = 1 ; i <= G.GraphSize() ; i++)
	{
		GetVertex(i).ClearVertex();
	}
	ExposedVetricesNbr = G.GetExpNbr();	
	delete [] Trees;
	Trees = new int[G.GetExpNbr()];
	int j = 0;
	for (auto i = 0; i < G.GraphSize(); i++)
	{
		if (G.VerticesState(i+1) == 0)
		{
			Trees[j] = i+1;
			j++;
		}
	}
	for (auto i = 0; i < ExposedVetricesNbr; i++)
	{
		GetVertex(Trees[i]).AddTotree(GetVertex(Trees[i]));
	}
}

Path::Path(Vertex& Sommet)
{
	VerticesList = NULL;
	Pathsize = Sommet.DistanceToRoot()+1;
	VerticesList = new int[Pathsize];
	assert ( VerticesList != NULL );
	Vertex* V = NULL;
	V = &Sommet;
	VerticesList[0] = V->VertexIndex();
	for (auto i = 1; i < Pathsize; i++)
	{
		V = V->GetFather();
		VerticesList[i] = V->VertexIndex();
	}
	V = NULL;
}

Path::Path(Path& chemin)
{
	Pathsize = chemin.PathSize();
	VerticesList = new int[Pathsize];
	assert ( VerticesList != NULL );
	for (auto i = 0; i < Pathsize; i++)
	{
		VerticesList[i] = chemin(i);
	}
}

Path::Path()
{
	VerticesList = NULL;
	Pathsize = 0;
}

Path::~Path()
{
	delete [] VerticesList;
}

int& Path::operator()(const std::size_t indice)
{
	assert (indice >= 0 && indice < Pathsize);
	return VerticesList[indice];
}

const int& Path::operator()(const std::size_t indice) const
{
	assert (indice >= 0 && indice < Pathsize);
	return VerticesList[indice];
}

int Path::PathSize()
{
	return Pathsize;
}

Path::Path(Vertex& Sommet_1, Vertex& Sommet_2)
{
	VerticesList = NULL;
	Path Path_1(Sommet_1) , Path_2(Sommet_2);
	if ( Sommet_1.Root() == Sommet_2.Root() )
	{
		assert ( Path_1(Path_1.PathSize()-1) == Path_2(Path_2.PathSize()-1) ); // S'assurer que la somme des deux graphe est un blossom
		cout << "On a trouvé un blossom" << endl;
		bool verif = true;
		int l1 = Path_1.PathSize();
		int l2 = Path_2.PathSize();
		int m1 , m2;
		while ( verif ) // cette boucle a pour but de laisser un seul sommet en commun entre les deux chemins
		{
			if ( Path_1(l1-1) == Path_2(l2-1) )
			{
				if (l1 > 1) // Pour protègé dans le cas ou l'un des sommets donnés à l'entrée de cette fonction est le tip du blossom (C'est rare mais possible)
				{
					l1--;
				}
				if (l2 > 1)
				{
					l2--;
				}
			}
			else
			{
				verif = false;
			}
		}
		// le pathsize d'un blossom compte le tip deux fois
		if (l1 > 1)
		{
			m1 = l1+1; // m1 est la distance entre le sommet_1 et le tip du blossom
		}
		else
		{
			assert (l1 == 1);
			m1 = 1;
		}
		if (l2 > 1)
		{
			m2 = l2+1; // m2 est la distance entre le sommet_2 et le tip du blossom
		}
		else
		{
			assert (l2 == 1);
			m2 = 1;
		}
		Pathsize = m1 + m2;
		cout << " Sa taille est : " << Pathsize << endl;
		VerticesList = new int[Pathsize];
		assert (VerticesList != NULL);
		for ( auto i = 0; i < m1 ; i++)
		{
			VerticesList[i] = Path_1(m1-1-i);
		}
		for ( auto i = 0; i < m2; i++)
		{
			VerticesList[m1+i] = Path_2(i);
		}
	}
	else // Dans le cas d'un augmenting path
	{
		assert ( Path_1(Path_1.PathSize()-1) != Path_2(Path_2.PathSize()-1) ); // S'assurer que c'est pas un blossom
		cout << "On a trouvé un chemin augmentant" << endl;
		Pathsize = Path_1.PathSize() + Path_2.PathSize();
		VerticesList = new int[Pathsize];
		assert (VerticesList != NULL);
		cout << "Il contient " << Pathsize << " sommets" << endl;
		for (auto i = 0 ; i < Path_1.PathSize() ; i++)
		{
			VerticesList[i] = Path_1(Path_1.PathSize()-1-i);
		}
		
		for (auto i = 0 ; i < Path_2.PathSize() ; i++)
		{
			VerticesList[i+Path_1.PathSize()] = Path_2(i);
		}
	}
}

int Path::Contains(int indice) // Elle renvoit si le sommet ayant l'indice décalé "indice" se trouve dans le chemin ou pas
{
	assert ( indice > 0 );
	int res = 0;
	for (auto i = 0; i < Pathsize; i++)
	{
		if (VerticesList[i] == indice)
		{
			res = 1;
			break;
		}
	}
	return res;
}

int Path::FindLinkedVertex(int indice , Graph& Graphe) // Cette fonction renvoit l'indice décalé du sommet du blossom lié au sommet ayant l'indice décalé "indice"
{
	int res = 0;
	for (auto i = 1 ; i <= Graphe.GraphSize() ; i++)
	{
		if ( Contains(i) == 1 && Graphe(i,indice) != 0 )
		{
			res = i;
			break;
		}
	}
	return res;
}

void Path::BuildPath(int N)
{
	assert (VerticesList == NULL);
	VerticesList = new int[N];
	assert ( VerticesList != NULL );
	Pathsize = N;
}

void Path::ReversePath()
{
	if (Pathsize > 1)
	{
		int P[Pathsize];
		for (auto i = 0 ; i < Pathsize ; i++)
		{
			P[i] = VerticesList[i];
		}
		for (auto i = 0 ; i < Pathsize ; i++)
		{
			VerticesList[i] = P[Pathsize-1-i];
		}
	}
}

Graph ReduiceGraph(Graph& OldGraph, Path& Blossom)
{  
	assert ( Blossom(0) == Blossom(Blossom.PathSize()-1) ); // Vérifier si c'est vraiment un blossom ou pas
	Graph NewGraph;
	int N = OldGraph.GraphSize() - Blossom.PathSize() + 2;
	NewGraph.BuildGraph(N);
	for (auto i = 1; i <= NewGraph.GraphSize(); i++)
	{
		NewGraph(i,NewGraph.GraphSize()) = 0; // Pour initialiser la derniere colonne du nouveau graphe
		NewGraph(NewGraph.GraphSize(),i) = 0;
	}
	int I = 1 , J = 1;
	for (auto i = 1; i <= OldGraph.GraphSize(); i++) // Nadi
	{
		if (Blossom.Contains(i) == 0)
		{ 
			NewGraph.OldInd(I) = i;
			for (auto j = 1; j <= OldGraph.GraphSize(); j++)
			{
				if (Blossom.Contains(j) == 0)
				{
					NewGraph(I,J) = OldGraph(i,j);
					J++;
				}
				else if (Blossom.Contains(j) == 1 && NewGraph(I,NewGraph.GraphSize()) < OldGraph(i,j))
				{
					NewGraph(I,NewGraph.GraphSize()) = OldGraph(i,j);
					NewGraph(NewGraph.GraphSize(),I) = OldGraph(i,j);
				}
			}
			// Remettre J à la 1er case de la nouvelle ligne
			I++;
			J = 1;
		}
	}
	int Edges_Nbr;
	int Exp_Vert_Nbr = NewGraph.GraphSize();
	for (auto i = 1; i <= NewGraph.GraphSize(); i++)
	{
		NewGraph(i,i) = 0;
		NewGraph.VerticesState(i) = 0;
		Edges_Nbr = 0;
		for (auto j = 1; j <= NewGraph.GraphSize(); j++)
		{
			if (NewGraph(i,j) != 0)
			{
				Edges_Nbr++;
				if (NewGraph(i,j)%3 == 0)
				{
					NewGraph.VerticesState(i) = j;
					Exp_Vert_Nbr--;
				}
			}
		}
		NewGraph(i,i) = Edges_Nbr;
	}
	NewGraph.SetExpNbr(Exp_Vert_Nbr);
	
	return NewGraph;
}

Path& FindPath(Path& Blossom, int FirstVertex, int SecondVertex, Graph& Graph)
{
	Path* result = new Path();
	if (FirstVertex == SecondVertex)
	{
		result->BuildPath(1); // Construction d'un chemin contenant un seul sommet
		(*result)(0) = FirstVertex;
	}
	else
	{
		int ScdVtxPst = 0; // On va chercher la position de SecondVertex
		while (Blossom(ScdVtxPst) != SecondVertex)
		{
			ScdVtxPst++;
		}
		assert (Blossom(ScdVtxPst) == SecondVertex && ScdVtxPst > 0 && ScdVtxPst < Blossom.PathSize()-1 );
		int Direction = 0;
		if ( Blossom(ScdVtxPst-1) == Graph.VerticesState(SecondVertex) )
		{
			Direction = -1;
			result->BuildPath(ScdVtxPst+1);
		}
		else
		{
			assert ( Blossom(ScdVtxPst+1) == Graph.VerticesState(SecondVertex) );
			Direction = 1;
			result->BuildPath(Blossom.PathSize()-ScdVtxPst);
		}
		for (auto i = result->PathSize()-1 ; i >= 0 ; i--)
		{
			if (Direction == -1)
			{
				(*result)(i) = Blossom(i);
			}
			else if ( Direction == 1 )
			{
				(*result)(i) = Blossom(Blossom.PathSize()-i-1);
			}
		}
		assert ( (*result)(0) == FirstVertex && (*result)(result->PathSize()-1) == SecondVertex );
	}
	return *result;
}

Path& MergePaths(Path& Path_1, Path& Path_2, int indice) // Cette fonction permet de remplacer la case d'indice "indice" dans Path_1 par Path_2
{
	Path* result = new Path();
	result->BuildPath(Path_1.PathSize()+Path_2.PathSize()-1);
	int j = 0;
	while (j != indice)
	{
		(*result)(j) = Path_1(j);
		j++;
	}
	for (auto i = 0 ; i < Path_2.PathSize() ; i++)
	{
		(*result)(j) = Path_2(i);
		j++;
	}
	int i = indice+1;
	while (j != result->PathSize())
	{
		(*result)(j) = Path_1(i);
		j++;
		i++;
	}
	return *result;
}

Path& AdjustPath(Graph& OldGraph , Graph& NewGraph , Path& Blossom , Path& OldAugPath)
{
	if (OldAugPath.PathSize() == 0)
	{
		Path* NewAugPath = new Path();
		return *NewAugPath;
	}
	else
	{
		//Path* NewAugPath = new Path();
		int FirstVertex = 0;
		int SecondVertex = 0;
		int verif = 0; // pour vérifier si le blossom fait partie de l'augmenting path
		for (auto i = 0; i < OldAugPath.PathSize(); i++)
		{
			if ( OldAugPath(i) != NewGraph.GraphSize() )
			{
				OldAugPath(i) = NewGraph.OldInd(OldAugPath(i));
			}
			else
			{
				verif = i+1; // Décalage pour la vérification après
			}
		}
		if ( verif != 0 ) // Si le blossom appartient à l'augmenting path
		{
			Path* BlossomPath = NULL;
			int CpldVertex = NewGraph.VerticesState(NewGraph.GraphSize()); // Le sommet couplé au blossom dans le NV graphe
			if ( CpldVertex == 0 ) // Alors le blossom est exposé
			{
				assert ( verif == 1 || verif == OldAugPath.PathSize() );
				FirstVertex = Blossom(0); // Le sommet exposé du blossom
				if ( verif == 1 )
				{
					SecondVertex = Blossom.FindLinkedVertex(OldAugPath(verif) , OldGraph); // c pr la ligne qu-dessous :j'ai remplace BlossomPath par BlossomP
					BlossomPath = &FindPath(Blossom, FirstVertex, SecondVertex, OldGraph); // trouver la partie du chemins augmentant qui coincide avec le blossom
				}
				else
				{
					SecondVertex = Blossom.FindLinkedVertex(OldAugPath(verif-2) , OldGraph);
					BlossomPath = &FindPath(Blossom, FirstVertex, SecondVertex, OldGraph);
					BlossomPath->ReversePath(); // Inverser le BlossomPath pour qu'il facilite la construction du nouveau chemin augmentant
				}
			}
			else
			{
				assert ( verif > 1 && verif < OldAugPath.PathSize() );
				int OldCpldVrtxInd = NewGraph.OldInd(CpldVertex); 	// L'ancien indice du sommet couplé avec le blossom
				assert ( OldCpldVrtxInd == OldAugPath(verif-2) || OldCpldVrtxInd == OldAugPath(verif) );
				FirstVertex = OldGraph.VerticesState(OldCpldVrtxInd); // L'indice du sommet du blossom couplé dans "OldGraph-Blossom"
				assert ( FirstVertex == Blossom(0) );
				if ( OldCpldVrtxInd == OldAugPath(verif-2) )
				{
					SecondVertex = Blossom.FindLinkedVertex(OldAugPath(verif) , OldGraph);
					BlossomPath = &FindPath(Blossom, FirstVertex, SecondVertex, OldGraph);
				}
				else
				{
					SecondVertex = Blossom.FindLinkedVertex(OldAugPath(verif-2) , OldGraph);
					BlossomPath = &FindPath(Blossom, FirstVertex, SecondVertex, OldGraph);
					BlossomPath->ReversePath();
				}
			}
			Path* NewAugPath = &MergePaths(OldAugPath, *BlossomPath, verif-1); // Cette fonction permet de remplacer la case d'indice "indice" dans Path_1 par Path_2
			delete BlossomPath;
			return *NewAugPath;
		}
		else
		{
			Path* NewAugPath = &OldAugPath; // Attention peut etre qu'il y'a une fuite mémoire grace a ça ou suppression deux fois de la mm mémoire, il faut testé ce cas
			return *NewAugPath;
		}
		//return *NewAugPath;
	}
}

Path& FindAugmentingPath(Graph& Graphe , Forest& Foret) // C'est la fonction principale chargée de trouver un chemin augmentant
{
	Vertex* V = NULL;
	Vertex* W = NULL;
	//Path* AugmentingPath = NULL;
	int UnMarkedVertex = Foret.GetUnMarkedVertex();
	int UnMarkedEdge = 0;
	cout << "On va commencé la recherche d'un chemin augmentant " << endl;
	if ( UnMarkedVertex != 0 )
	{
		cout << "Dans ce graphe il y'a des sommets non marqué" << endl;
	}
	while ( UnMarkedVertex != 0 )
	{
		V = &Foret.GetVertex(UnMarkedVertex);
		UnMarkedEdge = V->GetUnMarkedEdge();
		cout << "Le sommet numéro : " << V->VertexIndex() << " n'est pas marqué " << endl;
		while ( UnMarkedEdge != 0 )
		{
			W = &Foret.GetVertex(UnMarkedEdge);
			if ( W->Root() == 0 )
			{
				W->AddTotree(*V);
				Vertex* U = &Foret.GetVertex(Graphe.VerticesState(W->VertexIndex()));
				U->AddTotree(*W);
				cout << " On va ajouter les sommet : " << W->VertexIndex() << " et " << U->VertexIndex() << " à l'arbre de " << V->Root() << endl;
			}
			else
			{
				if ( W->DistanceToRoot() % 2 == 1 ) {cout << UnMarkedEdge << " est un sommet inner, donc on va rien faire" << endl;}
				else if ( V->Root() == W->Root() )
				{
					Path* Blossom = new Path(*W,*V);
					Path* ReduicedAugPath = NULL; // A voir avec l'attention de adjustePath aussi (chercher attention de la fonction AdjustePath)
					for (auto i = 0; i < Blossom->PathSize(); i++)
					{
						cout << (*Blossom)(i) << " ";
					}
					cout << endl;
					cout << " On va construire un nouveau graphe en réduisant ce blossom est vérifier s'il contient un augmenting path " << endl;
					Graph ReduicedGraph = ReduiceGraph(Graphe, *Blossom);
					cout << "Voici le graphe réduit " << endl;
					ReduicedGraph.DisplayGraph();
					Forest ReduicedForest( ReduicedGraph );
					ReduicedAugPath = &FindAugmentingPath(ReduicedGraph , ReduicedForest);
					if ( ReduicedAugPath->PathSize() == 0 )
					{
						cout << "On a rien trouver dans le graphe réduit" << endl; 
					}
					Path* AugmentingPath = &AdjustPath(Graphe , ReduicedGraph , *Blossom , *ReduicedAugPath);
					cout << "Le chemin augmentant ajusté est :" << endl;
					for (auto i=0; i < AugmentingPath->PathSize() ; i++)
					{
						cout << (*AugmentingPath)(i) << " ";
					}
					cout << endl;
					delete Blossom;
					if (AugmentingPath != ReduicedAugPath) // Voir Attention de AdjustePath, ce cas doit etre testé ( cad un blossom qui ne fait pas partie de l'augmenting path du graphe réduit
					{
						delete ReduicedAugPath;
					}
					return *AugmentingPath;
				}
				else
				{
					Path* P = new Path(*W,*V);
					for (auto i = 0; i < P->PathSize(); i++) // Affichage à supprimé
					{
						cout << (*P)(i) << " ";
					}
					cout << endl;
					return *P ;
				}
			}
			V->MarkEdge(W->VertexIndex());
			UnMarkedEdge = V->GetUnMarkedEdge();
		}
		V->MarkVertex();
		UnMarkedVertex = Foret.GetUnMarkedVertex();
	}
	cout << " Il reste aucun augmenting path " << endl;
	Path* AugmentingPath = new Path();
	return *AugmentingPath;
}

void IncreaseGraphMatching(Graph& Graphe, Path& AugmentingPath)
{
	int VtrxNbrAugPath = AugmentingPath.PathSize();
	assert ( VtrxNbrAugPath != 0 && VtrxNbrAugPath % 2 == 0 );
	for ( auto i = 0 ; i <= VtrxNbrAugPath - 2 ; i += 2 )
	{
		Graphe.IncreaseMatching( AugmentingPath(i) , AugmentingPath(i+1) );
	}
}

void FindMaximumMatching(Graph& Graphe)
{
	Forest Foret(Graphe);
	Path* AugmentingPath = NULL; // new Path();
	AugmentingPath = &FindAugmentingPath( Graphe , Foret );
	while (AugmentingPath->PathSize())
	{
		IncreaseGraphMatching(Graphe, *AugmentingPath);
		delete AugmentingPath;
		AugmentingPath = NULL;
		Foret.UpdateForest(Graphe);
		AugmentingPath = &FindAugmentingPath( Graphe , Foret );
	}
	delete AugmentingPath; //Pour libérer la mémoire allouée poue le dernier chemin augmentant (Il est vide mais on lui a alloué de la mémoire)
}

void ExportGraph(Graph& Graphe) // Une fonction qui permet de enregistrer le résultat de l'algorithme dans un fichier lisible par le logiciel graphviz
{
	std::ofstream file("../GraphMatching.txt");
	file << "graph GraphMatching { " << std::endl;
	for(auto i = 0; i < Graphe.GraphSize(); i++)
	{
		
		file << i << " [shape = point, color = blue];" << std::endl;
	}
	for(auto i = 0; i < Graphe.GraphSize(); i++)
	{
		for (auto j = i+1; j < Graphe.GraphSize(); j++)
		{
			if ( Graphe(i+1,j+1) == 1 )
			{
				file << i << "--" << j << ";" << std::endl;
				
			}
			else if ( Graphe(i+1,j+1) == 3 )
			{
				
				file << i << "--" << j <<"[color=red,penwidth=3.0];" << std::endl;
			}
		}
	}
	file << "}" << std::endl;
}



