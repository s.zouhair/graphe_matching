#ifndef DEF_Edmonds
#define DEF_Edmonds

class Graph {
  public :
  	Graph(const std::string GraphPath);
  	Graph();
	Graph(int , int );
	~Graph();
	std::size_t GraphSize() const;
	std::size_t GetExpNbr() const;
	int& operator()(const std::size_t, const std::size_t);
	const int& operator()(const std::size_t, const std::size_t) const;
	int& VerticesState(const std::size_t);
	void IncreaseMatching(const int& , const int&); // Cette fonction permet d'ajouter une arete au couplage
	void DecreaseMatching(const int&);  // Cette fonction permet d'enlever une arete au couplage
	void SetExpNbr(int);
	void BuildGraph(int);
	int& OldInd(int);
	void DisplayGraph();
  private :
	std::size_t VerticesNbr;  	
	int** GraphEdges;  // la case GraphEdges(i,i) a pour but de stocké le nombre d'arête du sommet "i"
	int* Vertices_State; // Ce tableau permet de stocker l'état des sommets (couplé "l'indice du sommet avec lequelle il est couplé" ou non "0").
	std::size_t ExposedVetricesNbr;
	int* OldIndex;
};

class Vertex {
  public :
	Vertex();
	Vertex(const Graph&, const std::size_t);
	~Vertex();
	std::size_t VertexIndex();
	int Root();
	int DistanceToRoot();
	void AddTotree(Vertex&); // On donne le sommet père en argument
	void AddSon(Vertex&);  // Cette fonction est appelée dans la fonction AddFather
	void AddFather(Vertex&); // Cette fonction est appelée dans la fonction AddTotree
	void MarkVertex();
	void UnMarkedDirect(Vertex&);
	void MarkedDirect(Vertex&); // Cette fonction est appelée automatiquement dans MarkVertex()
	int GetUnMarkedEdge(); // Cette fonction renvoit l'indice du sommet de l'autre extrémité d'une arete non marquée, si elle n'a trouvée aucune arete non marquée, elle renvoit 0
	void MarkEdge(int);
	Vertex* GetFather();
	int GetUnMarkedVertex();
	void ClearVertex();
  private :
	int** VertexEdges; // la 1er ligne contient les indices des sommet lié à "Vertex" par des aretes. la 2eme ligne permet de savoir si l'arete est marqué ou non. la 3eme si le sommet ne fait pas partie du meme arbre "0", si oui, il est soit père "1" soit fils "2". Et la 4ème si il y'a un sommet non marker par cette direction.
	int EdgesNbr; // le nombre des aretes du sommet courant
	std::size_t Index; // >= 1
	int VertexTree[3]; // Si le sommet appartient à un arbre, la 1er case de ce tableau va contenir son origine (root) et la 2eme son distance par rapport à cette origine. La 3eme case permet de savoir si le sommet est marqué ou non.
	Vertex* Father;

};


class Forest {
  public :
	Forest(Graph&);
	~Forest();
	Vertex& GetVertex(const std::size_t);
	int GetUnMarkedVertex(Vertex&);
	int GetUnMarkedVertex();
	void UpdateForest(Graph&);
  private :
	int* Trees; // Ce tableau contient les indices décalés des sommets exposés
	Vertex** VertexSet;
	std::size_t ExposedVetricesNbr;
	std::size_t VerticesNbr;

};

class Path {
  public :
	Path(Vertex&);
	Path(Path&);
	Path();
	Path(Vertex& , Vertex&); // Dans le cas d'un blossom ou un augmenting path
	~Path();
	int PathSize();
	int& operator()(const std::size_t);
	const int& operator()(const std::size_t) const;
	int Contains(int);
	int FindLinkedVertex(int , Graph&);
	void BuildPath(int);
	void ReversePath();

  private :
	int* VerticesList; // Liste des indices décalés des sommetes du chemin
	int Pathsize; // Le nombre de sommet

};

Path& MergePaths(Path& , Path& , int);

Graph ReduiceGraph(Graph& , Path&);

Path& FindPath(Path& , int , int , Graph&);

Path& AdjustPath(Graph& , Graph& , Path& , Path&);

Path& FindAugmentingPath(Graph& , Forest&);

void IncreaseGraphMatching(Graph& , Path&);

void FindMaximumMatching(Graph&);

void ExportGraph(Graph&); // Une fonction qui permet de enregistrer le résultat de l'algorithme dans un fichier lisible par le logiciel graphviz


#endif
