----------------------------------------------------------------------------------------
                                 Graph maximum matching
----------------------------------------------------------------------------------------

Ce programme permet de trouver un couplage maximum dans un graphe 
non orienté. Il est basé sur l’algorithme de Jack Edmonds publié en 1965.
Ce programme permet non seulement de trouver la solution pour un graphe non 
couplé, mais aussi d'améliorer un couplage non maximum déjà établie

Le graphe doit être décrit dans un fichier « graph.txt » placé dans 
le même répertoire que l’exécutable « main.exe », la structure de ce fichier 
est la suivante :


                                 (1)                         (2)
                                    O o o o o o o o o o o o O
	                         o  o o                     o  o
                              o     o   o                   o     o
                           o        o     o                 o        o
                        o           o       o               o           o
                     o              o         o             o              o
             (3)  O                 o           o           o                 O  (4)
                     o              o             o         o              o
                        o           o               o       o           o
                           o        o                 o     o        o
                              o     o                   o   o     o
                                 o  o                     o o  o
                                    O o o o o o o o o o o o O
                                 (5)                         (6)

Considérons le graphe ci-dessus :

	- La première ligne du fichier doit contenir le nombre n des 
	sommets, par exemple dans ce graphe n = 6.

	- Après, il y'a une matrice adjacente carré (n*n) qui indique 
	les connexions entre les sommets du graphe, voici la matrice du 
	graphe de l'exemple
	
				0  1  1  0  1  1
				1  0  0  1  0  1
				1  0  0  0  1  0
				0  1  0  0  0  1
				1  0  1  0  0  1
				1  1  0  1  1  0

Il faut noter que si le graphe possède déjà un couplage non maximum
avant l'exécution du programme, il faut remplacer les "1" des arêtes du 
couplage par 3. Dans l'exemple précédent, si les arêtes [(2)-(6)] et 
[(1)-(5)] appartiennent au couplage, la matrice adjacente devient :

				0  1  1  0  3  1
				1  0  0  1  0  3
				1  0  0  0  1  0
				0  1  0  0  0  1
				3  0  1  0  0  1
				1  3  0  1  1  0

Le programme renvoi à la fin un fichier « GraphMatching.txt »
contenant la description du graphe et son couplage, ce fichier est à utiliser
pour la visualisation du graphe sous Graphviz, mais avant de passer à cette 
étape, il est nécessaire de changer l'extension de ce fichier :

	- Sous WINDOWS :
		cp GraphMatching.txt GraphMatching.gv

	- Sous LINUX :
		cp GraphMatching.txt GraphMatching.dot

Pour visualiser le graphe et son couplage sous Windows, il suffit de charger 
le fichier « GraphMatching.gv » à partir de Graphviz. La visualisation sous 
Linux se fait en ligne de commande :

	dot -Tjpg -o GraphMatching.jpg GraphMatching.dot

Cette commande permet de créer une image jpg du graphe avec son couplage. Il 
faut mentionner qu'il est possible d'utilisé d'autres format d'image.

